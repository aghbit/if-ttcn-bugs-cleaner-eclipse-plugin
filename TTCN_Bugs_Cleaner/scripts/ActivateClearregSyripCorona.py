
# This script determines if SYRIP and CORONA checks are performed before and after every testcase in functions
# f_Check_Before and f_Check_After respectively. The argument of the script is .ttcn file.

from sys import argv
from TestCasesStructure import describes_tc_structure_part


def error(error_msg):
    print('Error:')
    print(error_msg)
    exit(1)


def in_comment(split_source, line_number, string):
    return is_in_single_comment(split_source[line_number], string) or is_in_comment_block(split_source, line_number)


def is_in_single_comment(line, string):
    ''' Retruns true if string is in single-line comment (after //) '''
    slash_index = line.find('//')
    if slash_index != -1 and string in line.lower()[slash_index + 2:]:
        return True
    return False


def is_in_comment_block(split_source, line_number):
    ''' Retruns true if line number is in comment block (in between /* and */) '''
    block_start = len(split_source) + 1
    for i in range(line_number, -1, -1):
        line = split_source[i]
        if '/*' in line:
            block_start = i
            break

    block_end = -1
    for i in range(block_start, len(split_source)):
        line = split_source[i]
        if '*/' in line:
            block_end = i
            break

    return block_start <= line_number <= block_end


def main():
    try:
        separator = '\\' if '\\' in argv[1] else '/'  # path separator
        filename = argv[1].split(separator)[-1]
        if not filename.endswith('.ttcn'):
            error('Argument of the script has to be a .ttcn file')
    except IndexError:
        error('Argument containing name of .ttcn file is necessary for the script.')

    try:
        file = open(argv[1], 'r')
    except FileNotFoundError:
        error('File not found.')

    split_source = file.read().split('\n')
    file.close()

    lines_with_defined_testcases = [line_number for line_number, line in enumerate(split_source, 1) if
                                    line.strip().startswith('testcase')]
    print('Lines:')

    for i, tc_line_number in enumerate(lines_with_defined_testcases):
        try:
            next_tc_line_number = lines_with_defined_testcases[i + 1]  # line, where next test case is defined
        except IndexError:
            next_tc_line_number = len(split_source)

        # lines, where given parts are definied, -1 means that it has not been found in the file
        line_with_precondition = -1
        line_with_test_part = -1
        line_with_restore = -1
        line_with_check_before = -1
        line_with_check_after = -1

        for j, line in enumerate(split_source[tc_line_number:next_tc_line_number]):

            # searching for line numbers of parts that are necessary for the script
            if describes_tc_structure_part(split_source, tc_line_number + j, 'PRECONDITION'):
                line_with_precondition = tc_line_number + j
            elif describes_tc_structure_part(split_source, tc_line_number + j, 'TEST'):
                line_with_test_part = tc_line_number + j
            elif (describes_tc_structure_part(split_source, tc_line_number + j, 'RESTORE') or
                  describes_tc_structure_part(split_source, tc_line_number + j, 'RESTORATION')):
                line_with_restore = tc_line_number + j
            elif 'f_check_before' in line.lower() and not in_comment(split_source, tc_line_number + j, 'f_check_before'):
                line_with_check_before = tc_line_number + j
            elif 'f_check_after' in line.lower() and not in_comment(split_source, tc_line_number + j, 'f_check_after'):
                line_with_check_after = tc_line_number + j

        # checking if precondition, test and restore parts exist
        if line_with_precondition == -1:
            print(str(tc_line_number) + ' precondition part not found')
        if line_with_test_part == -1:
            print(str(tc_line_number) + ' test part not found')
        if line_with_restore == -1:
            print(str(tc_line_number) + ' restore part not found')

        # checking if function f_check_before exists and is performed in precondition part
        if line_with_check_before == -1:
            print(str(tc_line_number) + ' function f_check_before not found')
        elif not (0 < line_with_precondition < line_with_check_before < line_with_test_part):
            print(str(tc_line_number) + ' function f_check_before is not in precondition part')

        # checking if function f_check_after exists and is performed in restore part
        if line_with_check_after == -1:
            print(str(tc_line_number) + ' function f_check_after not found')
        elif line_with_restore > line_with_check_after:
            print(str(tc_line_number) + ' function f_check_after is not in restore part')


if __name__ == '__main__':
    main()
