
# This script corrects an alignment of all lines in the file. In particular, it can:
# - correct the number of spaces in the beggining of each line
# - give the same indentation level to arguments of functions
# Argument of the script is the file that should be checked. In return, user gets a corrected file with the same name.

from sys import argv


def error(error_msg):
    print('Error:')
    print(error_msg)
    exit(1)


def between(string, index, symbol):
    """
    Returns true if character with given index in given string is between given symbol
    >>> between('a"bcddeff"', 3, '"')  # is 'c' between " in the string?
    True
    """
    return symbol in string[:index] and symbol in string[index + 1:]


def in_comment(string, index):
    """ Returns true if character with the given index of given string is in the comment ( <==> after // or # ) """
    comment_index1 = string.rfind('#')
    comment_index2 = string.rfind('//')

    # checking if comment symbols are not between " (are not characters from some string declared in the line) and then
    # if index of comment symbol is lower than index given as an argument
    return (not between(string, comment_index1, '"') and not between(string, comment_index2, '"') and
            (0 <= comment_index1 < index or 0 <= comment_index2 < index))


def main():
    try:
        separator = '\\' if '\\' in argv[1] else '/'  # path separator
        filename = argv[1].split(separator)[-1]
        if not filename.endswith('.ttcn'):
            error('Argument of the script has to be a .ttcn file')
    except IndexError:
        error('Argument containing name of .ttcn file is necessary for the script.')

    try:
        file = open(argv[1], 'r')
    except FileNotFoundError:
        error('File not found.')

    split_source = file.read().split('\n')
    file.close()

    result = ''  # corrected lines will be appended to this variable
    nesting_level = 0  # how many spaces are in the current nesting level
    stack = [0]  # stack necessary to remember nesting levels if arguments of function are placed in new lines

    for i, line in enumerate(split_source):

        if line.startswith('#') or line.startswith('/'):  # skip comments
            result += line
        else:
            if '}' in line and '{' not in line:  # <==> when only } is in the line
                nesting_level -= 2

            if line.lstrip().startswith(')'):
                nesting_level -= 2

            strip_line = line.strip()
            corrected_line = nesting_level * ' ' + strip_line   # adjusting line to correct nesting level
            result += corrected_line

            # managing round brackets
            last_opening_bracket = 0  # index of the last opening bracket in the line
            for index, char in enumerate(strip_line):
                if not between(strip_line, index, '"') and not in_comment(strip_line, index):
                    if char == '(':
                        stack.append(nesting_level)  # pushing currect nesting level to stack
                        nesting_level += index - last_opening_bracket + 2  # changing nesting level according to ( index
                        last_opening_bracket = index
                    elif char == ')':
                        if stack:
                            nesting_level = stack.pop()  # popping value from the stack, which is current nesting level

            if '{' in line and '}' not in line:   # when only { is in the line, nesting level increases by 2 spaces
                nesting_level += 2

        result += '\n'

    with open(argv[1][:-5] + '_aligned.ttcn', 'w') as corrected_file:
        corrected_file.write(result)

    print('Error:')  # to tell the plugin that the script is done
    print('Aligned file was created.')


if __name__ == '__main__':
    main()
