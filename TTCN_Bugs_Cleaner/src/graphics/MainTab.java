package graphics;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;

public class MainTab {
	private ScriptsComposite scripts;
	
	public Composite getContent( final Shell shell, TabFolder tabFolder, final Model model ){
		
		Composite mainComposite;
		Composite mainButtonsComposite;
	    final Button selectAll;
	    Button selectPrevious;
	    Button run;
	    
		mainComposite = new Composite(tabFolder, SWT.NONE);
		mainComposite.setLayout(new MigLayout("fill"));

		scripts = new ScriptsComposite(mainComposite, SWT.NONE);
		
		mainButtonsComposite = new Composite(mainComposite, SWT.NONE);
		mainButtonsComposite.setLayout(new MigLayout("fill"));
		
		selectAll = new Button(mainButtonsComposite, SWT.CHECK);
		selectPrevious = new Button(mainButtonsComposite, SWT.CHECK);
		run = new Button(mainButtonsComposite, SWT.PUSH);
		selectAll.setText("Select all");
		selectPrevious.setText("Select previously used");
		run.setText("Run scripts");
		
		scripts.setLayoutData("pos 0% 0% 100% 85%, hmin 0");	//thanks to hmin 0 the scrollbar will appear and component won't stretch out of gui
		mainButtonsComposite.setLayoutData("pos 0% 85% 100% 100%");
		selectAll.setLayoutData("cell 0 1 1 1");
		selectPrevious.setLayoutData("cell 1 1 2 1");
		run.setLayoutData("cell 0 2 2 1");
		

		
		selectAll.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				if(selectAll.getSelection())
					scripts.setAllChecked(true);
				else
					scripts.setAllChecked(false);
			}
		});
		
		selectPrevious.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				scripts.setChosenChecked( model.LastScriptsGetter() );
			}
		});
		
		run.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				String[] checkedScripts = scripts.getCheckedElements();
				
				model.runScripts( checkedScripts );
				model.saveDefaults( checkedScripts );
			}
		});
		
		return mainComposite;
	}

	public ScriptsComposite getScripts(){
		return this.scripts;
	}
}
