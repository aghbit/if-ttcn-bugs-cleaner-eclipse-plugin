package graphics;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class MessageDialog {
	
	public static Shell shell;

	public static void showMessage(String message){
		MessageBox messageDialog = new MessageBox(shell, SWT.OK);
		
		messageDialog.setText("Message");
		messageDialog.setMessage(message);
		messageDialog.open();
	}
}
