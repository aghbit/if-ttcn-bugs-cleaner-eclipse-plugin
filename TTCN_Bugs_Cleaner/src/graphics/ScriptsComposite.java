package graphics;

import java.io.File;
import java.util.ArrayList;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public class ScriptsComposite extends Composite{

	ArrayList <Button> scripts = new ArrayList <Button>();
	ArrayList <String> paths = new ArrayList <String>();
	Composite parent;
	
	public ScriptsComposite(Composite parent, int style) {
		super(parent, style);
		this.parent = parent;
		this.setLayout(new MigLayout());
	}

	public void updateScripts( String [] scriptsPaths ){
		
		for(Button b: scripts)
			b.dispose();
		
		scripts.removeAll( scripts );
		paths.removeAll( paths );
		this.redraw();
		this.pack();
		
		if (scriptsPaths != null){
			for( int i = 0; i < scriptsPaths.length; i++){
				Button b = new Button(this, SWT.CHECK | SWT.WRAP);
				b.setText( (new File( scriptsPaths[i] )).getName());
				b.setLayoutData("wrap");
				paths.add( scriptsPaths[i] );
				scripts.add(b);
			}
		}

		this.redraw();
		this.pack();
	}
	
	
	public String [] getCheckedElements(){
		ArrayList <String> checked = new ArrayList<String>();
		for(int i = 0 ; i < scripts.size() ; i++){
			if(scripts.get(i).getSelection() )
				checked.add(paths.get(i));
		}
		return checked.toArray(new String[checked.size()]);
	}
	
	
	/**
	 * Method for changing state of all scripts.
	 * */
	public void setAllChecked(boolean bool){
		for( Button b: scripts)
			b.setSelection(bool);
	}
	
	
	/**
	 * Method for changing state of chosen scripts.
	 * Created mainly for selecting last used scripts.
	 * */
	public void setChosenChecked(String [] chosen){
		for( String s: chosen){
			int index = paths.indexOf(s);
			if( index != -1){
				Button b = scripts.get(index);
				b.setSelection( !b.getSelection());
			}
		}
	}
}