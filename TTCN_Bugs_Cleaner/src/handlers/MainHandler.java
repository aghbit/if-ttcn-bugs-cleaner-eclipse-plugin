package handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import graphics.Model;
import graphics.View;
import graphics.Controller;
import graphics.MessageDialog;

public class MainHandler extends AbstractHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		
		// This would change icon of every dialog in Eclipse to ours. I can undestand why, but I can't figure out how to change just in ours
		//Window.setDefaultImage( new Image( window.getShell().getDisplay(), CoreSupport.getPath("resources/MainIcon.png") ) );
		
		Model model = new Model();
		MessageDialog.shell = window.getShell();
		View view = new View(window.getShell(), model);
		
		Controller controller = new Controller(model, view);
		if(app.Activator.Running){
			app.Activator.Running = false;
			controller.startView();
		}
		return null;
	}
}
