package handlers;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import graphics.MessageDialog;


public class MarkerSupport{
	
	/**Method that returns iFile that is presently open in the editor
	 * @return  iFile that is presently open in the editor
	 * @throws FileNotFoundException throws an exception when there is no file open in the editor(or the file in the editor isn't convertable to Ifile)
	 */
	private IFile getActiveIFile() throws NullPointerException {
		
		IWorkbenchPart workbenchPart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActivePart(); 
		IFile file = (IFile) workbenchPart.getSite().getPage().getActiveEditor().getEditorInput().getAdapter(IFile.class);
		
		return file;
	}
	
	
	/**Method that returns path to the file presently open in the editor
	 * @return path to the file presently open in the editor
	 * @throws FileNotFoundException throws an exception when there is no file open in the editor(or the file in the editor isn't convertable to Ifile)
	 */
	public String getPathToActiveIFile() throws NullPointerException{
		
		IWorkbenchPart workbenchPart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActivePart(); 
		IFile file = (IFile) workbenchPart.getSite().getPage().getActiveEditor().getEditorInput().getAdapter(IFile.class);
		
		return file.getRawLocation().toOSString();
	}
	
	/**
	 * Method that marks single file using ttcnbugsfindermarker
	 * @param file Ifile to be marked
	 * @param line line to be marked
	 * @param PathToScript path to script that caused marking
	 * @throws CoreException NullPointerException
	 */
	private void writeMarkers(IFile file,int line,String PathToScript,String warning)throws NullPointerException, CoreException{
			IMarker marker = file.createMarker(
					 "com.ttcn.ttcnbugsfindermarker"); //Tried to change the id to more appropriate but failed
			marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_WARNING);
			marker.setAttribute(IMarker.MESSAGE, warning);
			marker.setAttribute(IMarker.LINE_NUMBER,line);
	    } 
	
	/**
	 * Method deletes all TTCNBUGSFINDERMARKERS
	 */
	private void deleteAllMarkers(){
		try {
			ResourcesPlugin.getWorkspace().getRoot().deleteMarkers("com.ttcn.ttcnbugsfindermarker", true, IResource.DEPTH_INFINITE);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	/**Method that gets all warnings and call writeMarkers() to mark them
	 * @param warnings array of warnings to be marked
	 */
	public void markAllWarnings(ArrayList<String[]> ArrayWarnings){
		
		try{
			this.deleteAllMarkers(); //firstly 
			IFile file=this.getActiveIFile();
			String[][] warnings=ArrayWarnings.toArray(new String[ArrayWarnings.size()][]);
			
			Arrays.sort(warnings, new Comparator<String[]>() {
			    public int compare(String[] s1, String[] s2) {
			        Integer line1 = Integer.parseInt(s1[2]);
			        Integer line2 = Integer.parseInt(s2[2]);
			        return line1.compareTo(line2);
			    }
			});
			
			for(String[] warning: warnings)
				writeMarkers(file,Integer.parseInt(warning[2]),warning[1],warning[3]); 
		}
		catch (IndexOutOfBoundsException | NumberFormatException | NullPointerException | CoreException e){
			MessageDialog.showMessage("No ttcn file open in the active editor");
		}
	}	
}